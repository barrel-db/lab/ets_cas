ets_cas
=====

handle CAS semantic to ETS on different OTP version.

Design
------

Erlang OTP only added something similar to CAS in OTP 20 and sup with the `ets:select_replace/2` function. 

This lib bring similar feature to other version by serializing the writes that need CAS inside a gen_server.

Each ETS table created is created inside  a gen_server with the same name. we provide a simple document API to retriee the value. 

Example of usage
----------------

```erlang
%% Create a table
{ok, _} = ets_cas:new(test),

%%  Create a new doc and store it.
{ok, Doc} = ets_cas:create_doc(test, <<"a">>, #{}, ""),

%% update the document and save changes
Doc2 = ets_cas:set_val("test", Doc),
{ok, _Doc3} = ets_cas:put_doc_cas(test, Doc2),

%% saving it twice will fail since the version of updated document changed
false = ets_cas:put_doc_cas(test, Doc2),

%% get the new document and retrieve its value
{ok, Doc4} = ets_cas:get_doc(test, <<"a">>),
"test" = ets_cas:get_val(Doc4). 
```


> Note: You can notice the map given when created a doc. This allows us to store a map associated to a key. Using
> `ets_cas:set_map/2` and `ets_cas:get_map/1` allows your application to use them. The functions `ets_cas:set_mapval/3`
> and `ets_cas:get_mapval/1` allows you to manipulate both in one pass.

Build
-----

    $ rebar3 compile
