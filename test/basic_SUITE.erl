-module(basic_SUITE).

%% API
%% API
-export([
  all/0,
  init_per_suite/1,
  end_per_suite/1,
  init_per_testcase/2,
  end_per_testcase/2
]).

-export([
  basic/1,
  delete/1
]).

all() ->
  [
    basic,
    delete
  ].


init_per_suite(Config) -> Config.

end_per_suite(Config) ->Config.

init_per_testcase(_, Config) -> Config.

end_per_testcase(_, _Config) -> ok.


basic(_Config) ->
  {ok, _} = ets_cas:new(test),
  {ok, Doc} = ets_cas:create_doc(test, <<"a">>, #{}, ""),
  Doc2 = ets_cas:set_val("test", Doc),
  {ok, _Doc3} = ets_cas:put_doc_cas(test, Doc2),
  false = ets_cas:put_doc_cas(test, Doc2),
  {ok, Doc4} = ets_cas:get_doc(test, <<"a">>),
  "test" = ets_cas:get_val(Doc4),
  ok = ets_cas:close(test),
  undefined = ets:info(test, size).

delete(_Config) ->
  {ok, _} = ets_cas:new(test),
  {ok, Doc} = ets_cas:create_doc(test, <<"a">>, #{}, ""),
  Doc2 = ets_cas:set_val("test", Doc),
  {ok, Doc3} = ets_cas:put_doc_cas(test, Doc2),
  error = ets_cas:delete_doc_cas(test, Doc),
  ok = ets_cas:delete_doc_cas(test, Doc3),
  ok = ets_cas:close(test).

